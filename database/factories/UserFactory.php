<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => !rand(0, 3) ? $faker->firstName : '',
        'title' => !rand(0, 3) ? $faker->title : '',
        'email' => $faker->unique()->safeEmail,
        'avatar' => 'something',
        'password' => Hash::make('letmein'),
        'remember_token' => str_random(10),
    ];
});
