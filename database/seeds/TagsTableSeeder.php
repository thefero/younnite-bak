<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

/**
 * Class TagsTableSeeder
 */
class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 100) as $index) {
            \App\Tag::create([
                'name' => $faker->word(),
            ]);
        }
    }
}
