<?php

use Illuminate\Database\Seeder;
use App\Repositories\CountryRepository;

/**
 * Class CountriesTableSeeder
 */
class CountriesTableSeeder extends Seeder
{
    /** @var CountryRepository $countryRepository */
    protected $countryRepository;

    /**
     * CountriesTableSeeder constructor.
     * @param CountryRepository $countryRepository
     */
    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->countryRepository->create([
            'name' => 'Romania'
        ]);
    }
}
