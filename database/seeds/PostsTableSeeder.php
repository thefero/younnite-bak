<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 15) as $index) {
            /** @var \App\Post $post */
            $post = \App\Post::create([
                'user_id'   => rand(1, 30),
                'what'      => 'project',
                'post_id'   => rand(1, 30),
            ]);

            $tags =[];
            for ($i = 0; $i < rand(0, 10); $i++) {
                $tags[] = rand(1, 100);
            }

            $post->tags()->sync($tags);
        };
    }
}
