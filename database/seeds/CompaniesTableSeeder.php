<?php

use App\Services\CompanyService;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompaniesTableSeeder extends Seeder
{
    /** @var CompanyService $companyService */
    private $companyService;

    /**
     * CompaniesTableSeeder constructor.
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cities = \App\City::all()->pluck('id')->toArray();

        foreach (range(1, 100) as $index) {
            shuffle($cities);

            $this->companyService->create([
                'creator_id' => rand(1, 30),
                'name' => $faker->company(),
                'cover' => __DIR__ . '/data/cover_images/cover' . rand(1, 24) . '.jpg',
                'city_id' => $cities[0],
            ]);
        }
    }
}
