<?php

use App\ActivityDomain;
use App\Company;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class CompanyActivityDomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Collection|Company[] $companies */
        $companies = Company::all();

        /** @var Collection|ActivityDomain[] $activityDomains */
        $activityDomains = ActivityDomain::all();

        /**
         * @var int $i
         * @var Company $company
         */
        foreach ($companies as $i => $company) {
            foreach ($activityDomains->shuffle()->slice(0, rand(3, 7)) as $activityDomain) {
                $currentDate = new DateTime();

                try {
                    $company->activityDomains()->save(
                        $activityDomain,
                        [
                            'created_at' => $currentDate,
                            'updated_at' => $currentDate
                        ]
                    );
                } catch (\Illuminate\Database\QueryException $exception) {}
            }
        }
    }
}
