<?php

use Illuminate\Database\Seeder;

/**
 * Class CollegesTableSeeder
 */
class CollegesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\City::where('name', 'Brasov')->first()
            ->highSchools()
            ->saveMany([
                new \App\College([ 'name' => 'Universitatea Transilvania' ]),
                new \App\College([ 'name' => 'Facultatea de Medicina' ]),
                new \App\College([ 'name' => 'Facultatea de Constructii' ]),
            ]);

        \App\City::where('name', 'Bucuresti')->first()
            ->highSchools()
            ->saveMany([
                new \App\College([ 'name' => 'Universitatea Bucuresti' ]),
                new \App\College([ 'name' => 'Facultatea de Științe Politice' ]),
                new \App\College([ 'name' => 'Facultatea de Stiinte Economice' ]),
            ]);

        \App\City::where('name', 'Timisoara')->first()
            ->highSchools()
            ->saveMany([
                new \App\College([ 'name' => 'Facultatea de Economie si de Administrare a Afacerilor' ]),
                new \App\College([ 'name' => 'Facultatea de Sociologie si Psihologie' ]),
                new \App\College([ 'name' => 'Facultatea de Mecanica' ]),
            ]);

        \App\City::where('name', 'Cluj')->first()
            ->highSchools()
            ->saveMany([
                new \App\College([ 'name' => 'Facultatea de Teologie Ortodoxa' ]),
                new \App\College([ 'name' => 'Facultatea de Automatica si Calculatoare' ]),
                new \App\College([ 'name' => 'Facultatea de Instalatti' ]),
            ]);

        \App\City::where('name', 'Iasi')->first()
            ->highSchools()
            ->saveMany([
                new \App\College([ 'name' => 'Facultatea de Economie și Administrarea Afacerilor' ]),
                new \App\College([ 'name' => 'Facultatea de Arhitectura G. M. Cantacuzino' ]),
                new \App\College([ 'name' => 'Facultatea de Psihologie si Stiinte ale Educatiei' ]),
            ]);
    }
}
