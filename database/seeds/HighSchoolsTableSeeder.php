<?php

use Illuminate\Database\Seeder;

/**
 * Class HighSchoolsTableSeeder
 */
class HighSchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\City::where('name', 'Brasov')->first()
            ->highSchools()
            ->saveMany([
                new \App\HighSchool([ 'name' => 'Colegiul National de Informatica, Grigore Moisil' ]),
                new \App\HighSchool([ 'name' => 'Colegiul National Andrei Saguna' ]),
                new \App\HighSchool([ 'name' => 'Colegiul National Ioan Mesota' ]),
            ]);

        \App\City::where('name', 'Bucuresti')->first()
            ->highSchools()
            ->saveMany([
                new \App\HighSchool([ 'name' => 'Colegiul National Gheorghe Lazar' ]),
                new \App\HighSchool([ 'name' => 'Colegiul National Spiru Haret' ]),
                new \App\HighSchool([ 'name' => 'Colegiul National Mihai Viteazu' ]),
            ]);

        \App\City::where('name', 'Timisoara')->first()
            ->highSchools()
            ->saveMany([
                new \App\HighSchool([ 'name' => 'Liceul Teoretic Grigore Moisil' ]),
                new \App\HighSchool([ 'name' => 'Liceul Pedagogic Carmen Sylva' ]),
                new \App\HighSchool([ 'name' => 'Liceul Teoretic William Shakespeare' ]),
            ]);

        \App\City::where('name', 'Cluj')->first()
            ->highSchools()
            ->saveMany([
                new \App\HighSchool([ 'name' => 'Liceul Victor Babes Cluj' ]),
                new \App\HighSchool([ 'name' => 'Liceul Teoretic Gheorghe Sincai' ]),
            ]);

        \App\City::where('name', 'Iasi')->first()
            ->highSchools()
            ->saveMany([
                new \App\HighSchool([ 'name' => 'Colegiul National' ]),
                new \App\HighSchool([ 'name' => 'Colegiul National Costache Negruzzi' ]),
            ]);

        \App\City::where('name', 'Fetesti')->first()
            ->highSchools()
            ->saveMany([
                new \App\HighSchool([ 'name' => 'Liceul Teoretic "Carol I"' ]),
                new \App\HighSchool([ 'name' => 'Liceul Tehnologic De Industrie Alimentara' ]),
                new \App\HighSchool([ 'name' => 'Grup Scolar Constructii Cai Ferate' ]),
            ]);
    }
}
