<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Collection;
use App\User;

/**
 * Class UserFriendsTableSeeder
 */
class UserFriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Collection|User[] $users */
        $users = \App\User::all();
        $userIds = range(0, 30);

        /**
         * @var int $i
         * @var User $user
         */
        foreach ($users as $i => $user) {
            $friendsIds = $userIds;
            unset($friendsIds[$i]);
            shuffle($friendsIds);

            foreach (array_slice($friendsIds, 0, rand(5, 18)) as $friendsId) {
                $currentDate = new DateTime();

                try {
                    $user->friends()->save(
                        $users[$friendsId],
                        [
                            'connection' => join('_', array_sort([ $user->id, $users[$friendsId]->id ])),
                            'created_at' => $currentDate,
                            'updated_at' => $currentDate
                        ]
                    );
                } catch (\Illuminate\Database\QueryException $exception) {}
            }
        }
    }
}
