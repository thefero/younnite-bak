<?php

use Illuminate\Database\Seeder;
use App\Services\ProjectService;
use Faker\Factory as Faker;

/**
 * Class ProjectsTableSeeder
 */
class ProjectsTableSeeder extends Seeder
{
    /** @var ProjectService $projectService */
    private $projectService;

    /**
     * ProjectsTableSeeder constructor.
     * @param ProjectService $projectService
     */
    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 30) as $index) {
            $futurePast = rand(1, 10);
            $futurePastFromNow = rand(1, 10);
            $addSubtract = ['-', '+'];
            $futureUnit = ['days', 'weeks', 'months'];
            $hours = range(1, 23);

            $currentDate = new DateTime();
            $startDate = DateTime::createFromFormat('Y-m-d H:i:s', $currentDate->format('Y-m-d H:i:s'))
                ->modify($addSubtract[rand(0, 1)] . ' ' . $futurePastFromNow . $futureUnit[rand(0, 2)]);

            $hasTime = rand(0, 1);
            $startTime = $hasTime ?
                $startDate->format('Y-m-d') . ' ' . $hours[rand(0, count($hours) - 1)] . ':00:00':
                null;

            $endDate = DateTime::createFromFormat('Y-m-d H:i:s', $startDate->format('Y-m-d H:i:s'))
                ->modify('+ ' . $futurePast . $futureUnit[rand(0, 2)]);
            $endTime = $hasTime ?
                $endDate->format('Y-m-d') . ' ' . $hours[rand(0, count($hours) - 1)] . ':00:00':
                null;

            /** @var \App\Project $project */
            $project = $this->projectService->create([
                'user_id'      => rand(1, 30),
                'title'        => $faker->sentence(5),
                'subtitle'     => $faker->sentence(10),
                'description'  => $faker->paragraph(2, true),
                'banner'       => __DIR__ . '/data/project_images/project' . rand(1, 24) . '.jpg',
                'start_date'   => $startDate,
                'start_time'   => $startTime,
                'end_date'     => $endDate,
                'end_time'     => $endTime,
            ]);

            $tags = [];
            for ($i = 0; $i < rand(0, 10); $i++) {
                $tags[] = rand(1, 100);
            }

            $project->tags()->sync($tags);
        }
    }
}
