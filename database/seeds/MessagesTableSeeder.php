<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

/**
 * Class MessagesTableSeeder
 */
class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 2000) as $index) {
            \App\Message::create([
                'author_id' => rand(1, 30),
                'receiver_id' => rand(1, 30),
                'created_at' => microtime(true),
                'updated_at' => microtime(true),
                'body' => $faker->sentence(15, true),
            ]);
        };
    }
}