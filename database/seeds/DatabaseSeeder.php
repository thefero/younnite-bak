<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('public/temp');
        Storage::makeDirectory('public/temp');
        Storage::deleteDirectory('public/user/cover');
        Storage::deleteDirectory('public/user/avatar');
        Storage::deleteDirectory('public/project/banners');
        Storage::deleteDirectory('public/company/cover');
        Storage::deleteDirectory('public/company/avatar');

        $this->call(TagsTableSeeder::class);
        $this->call(ActivityDomainsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CompanyActivityDomainsTableSeeder::class);
        $this->call(HighSchoolsTableSeeder::class);
        $this->call(CollegesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserFriendsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
    }
}
