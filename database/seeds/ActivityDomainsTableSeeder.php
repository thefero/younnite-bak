<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ActivityDomainsTableSeeder
 */
class ActivityDomainsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activityDomains = Yaml::parse(file_get_contents(__DIR__ . '/data/activity_domains.yml'));

        foreach ($activityDomains as $activityDomain) {
            \App\ActivityDomain::create([
                'name' => $activityDomain['name']
            ]);
        }
    }
}
