<?php

use Illuminate\Database\Seeder;
use App\Services\UserService;
use Faker\Factory as Faker;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /** @var UserService $user */
    protected $userService;

    /**
     * UsersTableSeeder constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $victor = $this->userService->create([
            'first_name'    => 'Victor',
            'last_name'     => 'Dumitru',
            'email'         => 'vctr.dumitru@gmail.com',
            'cover'         => __DIR__ . '/data/cover_images/cover' . rand(1, 38) . '.jpg',
            'password'      => Hash::make('letmein'),
        ]);

        $victor->highSchools()->sync(
            \App\HighSchool::where('name', 'Colegiul National de Informatica, Grigore Moisil')->get()
        );

        $victor->colleges()->sync([
            \App\College::where('name', 'Universitatea Bucuresti')->first()->id,
            \App\College::where('name', 'Universitatea Transilvania')->first()->id,
        ]);

        foreach (range(1, 30) as $index) {
            $this->userService->create([
                'first_name'  => $faker->firstName(),
                'last_name'   => $faker->lastName(),
                'title'       => !rand(0, 3) ? $faker->title() : '',
                'middle_name' => !rand(0, 3) ? $faker->firstName() : '',
                'cover'       => __DIR__ . '/data/cover_images/cover' . rand(1, 38) . '.jpg',
                'email'       => $faker->email(),
                'password'    => Hash::make('letmein'),
            ]);
        }
    }
}
