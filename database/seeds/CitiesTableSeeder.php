<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $romania = \App\Country::find(1);

        $romania->cities()->saveMany([
            new \App\City([ 'name' => 'Brasov' ]),
            new \App\City([ 'name' => 'Bucuresti' ]),
            new \App\City([ 'name' => 'Timisoara' ]),
            new \App\City([ 'name' => 'Cluj' ]),
            new \App\City([ 'name' => 'Iasi' ]),
            new \App\City([ 'name' => 'Fetesti' ]),
        ]);
    }
}
