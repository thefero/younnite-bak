<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreMessagePost
 * @package App\Http\Requests
 */
class StoreMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * set the validation error messages.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'numeric' => 'The \':attribute\' property must be a valid integer value',
            'to.exists' => 'Invalid receiver',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'message' => 'required',
            'to' => 'required|numeric|exists:users,id',
        ];
    }
}
