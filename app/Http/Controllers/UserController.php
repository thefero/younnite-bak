<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends ApiController
{
    /** @var UserRepository $user */
    private $user;

    /** @var UserService $userService */
    private $userService;

    /**
     * UserController constructor.
     *
     * @param UserRepository $user
     * @param UserService $userService
     */
    public function __construct(UserRepository $user, UserService $userService)
    {
        parent::__construct();

        $this->user = $user;
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return UserResource
     */
    public function self(Request $request): UserResource
    {
        return UserResource::make(
            $request->user()->load('avatars', 'covers')
        );
    }

    /**
     * @param Request $request
     * @return UserCollection
     */
    public function friends(Request $request): UserCollection
    {
        return UserCollection::make(
            $this->userService->getFriendsWithLastMessage($request->url(), 20, $request->all())
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return UserResource
     */
    public function show(Request $request, int $id): UserResource
    {
        return UserResource::make(
            $this->user->find($id, $this->includes, $request->all())->load('avatars', 'covers')
        );
    }
}
