<?php

namespace App\Http\Controllers;

use App\Http\Resources\CompanyCollection;
use App\Repositories\CompanyRepository;

/**
 * Class CompanyController
 * @package App\Http\Controllers
 */
class CompanyController extends Controller
{
    /** @var CompanyRepository $company */
    private $company;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->company = $companyRepository;
    }

    /**
     * @return CompanyCollection
     */
    public function list(): CompanyCollection
    {
        return CompanyCollection::make($this->company::all());
    }
}
