<?php

namespace App\Http\Controllers;

use App\Events\MessageCreated;
use App\Http\Requests\StoreMessage;
use App\Http\Resources\MessageCollection;
use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;
use App\Repositories\MessageRepository;
use App\User;
use Illuminate\Support\Facades\Input;

/**
 * Class MessageController
 * @package App\Http\Controllers
 */
class MessageController extends ApiController
{
    /** @var MessageRepository */
    protected $message;

    /**
     * MessageController constructor.
     * @param MessageRepository $message
     */
    public function __construct(MessageRepository $message)
    {
        parent::__construct();

        $this->message = $message;
    }

    /**
     * @return MessageCollection
     */
    public function list(): MessageCollection
    {
        $with = Input::get('with');

        $partner = $with ?
            User::where(is_numeric($with) ? 'id' : 'email', $with)->firstOrFail()
            : null;

        $messages = MessageCollection::make($this->message::myMessages($_GET, $partner));

        return $partner ?
            $messages->additional([
                'participant' => UserResource::make($partner),
            ]) :
            $messages;
    }

    /**
     * @param StoreMessage $request
     * @return MessageResource
     */
    public function create(StoreMessage $request): MessageResource
    {
        $createdAt = $updatedAt = microtime(true);

        $message = $this->message::create([
            'receiver_id' => $request->get('to'),
            'body' => $request->get('message'),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ]);


        event(new MessageCreated($message, \Auth::user()));

//        return MessageResource::make($message->load([ 'receiver' ]));
        return MessageResource::make($message);
    }
}
