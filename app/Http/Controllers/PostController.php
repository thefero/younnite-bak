<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePost;
use App\Http\Resources\PostCollection;
use App\Services\PostService;
use Illuminate\Http\Request;

/**
 * Class PostsController
 * @package App\Http\Controllers
 */
class PostController extends ApiController
{
    /**
     * @param PostService $postService
     * @param Request $request
     * @return PostCollection
     */
    public function list(PostService $postService, Request $request): PostCollection
    {
        return PostCollection::make($postService->all($request->url(), $request->all()));
    }

    /**
     * @param CreatePost $request
     */
    public function create(CreatePost $request)
    {
        print_r($request->all());
    }
}
