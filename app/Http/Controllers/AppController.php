<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class AppController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        return view('app', [
            'user' => UserResource::make(\Auth::user()->load('avatars', 'covers'))->toArray($request)
        ]);
    }
}
