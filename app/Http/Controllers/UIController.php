<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class UIController
 * @package App\Http\Controllers
 */
class UIController extends Controller
{
    /**
     * @return View
     */
    public function __invoke(): View
    {
        return view('ui');
    }
}
