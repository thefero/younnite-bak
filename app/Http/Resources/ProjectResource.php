<?php

namespace App\Http\Resources;

use App\Image;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [
            'id' => $this->id,
        ];

        if ($this->relationLoaded('banners')) {
            $data['banner'] = [];

            /** @var Image $banner */
            foreach ($this->resource->banners as $banner) {
                $data['banner']['_' . $banner->width] = $banner->url;
            }
        }

        $data = array_merge($data, [
            'author' => UserResource::make($this->author),
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'description' => $this->description,
            'created_at' => $this->created_at->toDateTimeString(),
            'start_date' => $this->start_date,
            'start_time' => $this->start_time,
            'end_date' => $this->end_date,
            'end_time' => $this->end_time,
        ]);

        if ($this->relationLoaded('tags')) {
            $data['tags'] = [];

            /** @var Tag $tag */
            foreach ($this->tags as $tag) {
                $data['tags'][] = $tag->name;
            }
        }

        return $data;
    }
}
