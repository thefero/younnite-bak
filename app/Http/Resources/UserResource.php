<?php

namespace App\Http\Resources;

use App\Image;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [
            'id'           => $this->id,
            'first_name'   => $this->first_name,
            'last_name'    => $this->last_name,
            'middle_name'  => $this->middle_name,
            'display_name' => $this->first_name . ' ' . $this->last_name,
            'title'        => $this->title,
            'email'        => $this->email,
        ];

        if (method_exists($this->resource, 'relationLoaded')) {
            if ($this->relationLoaded('lastMessage')) {
                $data['last_message'] = MessageResource::make($this->getRelation('lastMessage'));
            }

            if ($this->relationLoaded('avatars')) {
                $data['avatar'] = [];

                /** @var Image $avatar */
                foreach ($this->resource->avatars as $avatar) {
                    $data['avatar']['_' . $avatar->width] = $avatar->url;
                }
            }

            if ($this->relationLoaded('covers')) {
                $data['cover'] = [];

                $converter = [
                    1110 => 'xl',
                    690  => 'l',
                    429  => 'm',
                    266  => 's',
                ];

                /** @var Image $avatar */
                foreach ($this->resource->covers as $cover) {
                    $data['cover'][$converter[$cover->width]] = $cover->url;
                }
            }

            if ($this->relationLoaded('posts')) {
                $collection = PostCollection::make($this->posts);
                $postsArr = $this->posts->toArray();

                $data['posts']['data'] = $collection;

                $data['posts']['links'] = [
                    'first' => $postsArr['first_page_url'] ?? null,
                    'last'  => $postsArr['last_page_url']  ?? null,
                    'prev'  => $postsArr['prev_page_url']  ?? null,
                    'next'  => $postsArr['next_page_url']  ?? null,
                ];

                $data['posts']['meta'] = Arr::except($postsArr, [
                    'data',
                    'first_page_url',
                    'last_page_url',
                    'prev_page_url',
                    'next_page_url',
                ]);
            }
        }

        return $data;
    }
}
