<?php

namespace App\Http\Resources;

use App\Image;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CompanyResource
 * @package App\Http\Resources
 */
class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        if ($this->relationLoaded('avatars')) {
            $data['avatar'] = [];

            /** @var Image $avatar */
            foreach ($this->resource->avatars as $avatar) {
                $data['avatar']['_' . $avatar->width] = $avatar->url;
            }
        }

        if ($this->relationLoaded('covers')) {
            $data['cover'] = [];

            /** @var Image $avatar */
            foreach ($this->resource->covers as $cover) {
                $data['cover']['_' . $cover->width] = $cover->url;
            }
        }

        $data = array_merge($data, [
            'city' => CityResource::make($this->whenLoaded('city')),
            'creator' => UserResource::make($this->whenLoaded('creator')),
        ]);

        $data['activity_domains'] = ActivityDomainCollection::make($this->whenLoaded('activityDomains'));

        return $data;
    }
}
