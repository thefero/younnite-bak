<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MessageResource
 * @package App\Http\Resources
 */
class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'logged_user_is_author' => boolval($this->author_id == $request->user()->id),
            'participant' => $this->when(
                $this->author_id != $request->user()->id,
                new UserResource($this->whenLoaded('author')),
                new UserResource($this->whenLoaded('receiver'))
            ),
            'body' => $this->body,
            'date' => \DateTime::createFromFormat('U.u', $this->created_at)->format('Y-m-d H:i:s.u'),
        ];
    }
}
