<?php

namespace App\Http\Resources;

use App\Tag;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [
            'id' => $this->id,
            'post_item' => $this->what,
            'post' => $this->what === 'project' || true ? new ProjectResource($this->post) : [],
            'author' => new UserResource($this->whenLoaded('author')),
        ];

        if ($this->relationLoaded('tags')) {
            $data['tags'] = [];

            /** @var Tag $tag */
            foreach ($this->tags as $tag) {
                $data['tags'][] = $tag->name;
            }
        }

        return$data;
    }
}
