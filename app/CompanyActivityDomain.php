<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class CompanyActivityDomain
 * @package App
 */
class CompanyActivityDomain extends Pivot
{
    protected $table = 'company_activity_domains';
}
