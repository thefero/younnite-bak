<?php

namespace App\Repositories;

use App\Post;
use Illuminate\Contracts\Pagination\Paginator;

/**
 * Class PostRepository
 * @package App\Repositories
 */
class PostRepository
{
    /**
     * @return Paginator
     */
    public static function all(): Paginator
    {
        return Post::with([ 'author.avatars', 'post.banners', 'tags', 'post.tags', 'post.author.avatars' ])->paginate(10);
    }
}