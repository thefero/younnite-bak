<?php

namespace App\Repositories;

use App\Project;

/**
 * Class ProjectRepository
 * @package App\Repositories
 */
class ProjectRepository
{
    /**
     * @param array $attributes
     * @return Project
     */
    public function create(array $attributes = []): ?Project
    {
        return Project::create($attributes);
    }
}
