<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\{
    Builder, Expression, JoinClause
};
use Illuminate\Support\Facades\{
    Auth, DB
};

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * @param int $id
     * @param array $includes
     * @param array $query
     * @return Model
     */
    public static function find(int $id, array $includes = [], array $query = []): Model
    {
        $user = User::find($id);

        foreach ($includes as $include) {
            switch ($include) {
                case 'posts':
                    $user->setRelation(
                        'posts',
                        $user->posts()->paginate(5)->appends($query)
                    );
                    break;
                default:
                    break;
            }
        }

        return $user;
    }

    /**
     * @param string $currentUrl
     * @param int $perPage
     * @param array $query
     * @return array
     */
    public function getFriendsWithLastMessage(string $currentUrl = '', int $perPage = 10, array $query = []): array
    {
        $search = isset($query['search']) && strlen($query['search']) ? $query['search'] : '';
        $page = isset($query['page']) && $query['page'] ? $query['page'] : 1;

        $users = DB::table('user_friends')
            ->select(DB::raw('
                SQL_CALC_FOUND_ROWS
                friend.id,
                friend.first_name,
                friend.last_name,
                friend.middle_name,
                friend.title,
                friend.email,
                messages.id as message_id,
                messages.author_id as message_author_id,
                messages.receiver_id as message_receiver_id,
                messages.body as message_body,
                messages.created_at as message_created_at'
            ))
            ->join('users', function (JoinClause $join) {
                $join->on('users.id', '=', 'user_friends.friend_id');
                $join->orOn('users.id', '=', 'user_friends.user_id');
            })
            ->join(
                'users as friend',
                'friend.id',
                '=',
                DB::raw('IF (user_friends.user_id = ' . Auth::user()->id .
                    ', user_friends.friend_id, user_friends.user_id)'))
            ->leftJoin('messages', 'messages.id', '=', $this->lastMessage())
            ->where('users.id', Auth::user()->id);

        if ($search) {
            $users = $users->where(function(Builder $query) use ($search) {
                $query->where('friend.first_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('friend.last_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('friend.middle_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('friend.email', 'LIKE', '%' . $search . '%');
            });
        }

        $users = $users
            ->orderBy('message_id', 'desc')
            ->skip(($page - 1) * $perPage)->take($perPage)
            ->get();

        $total = DB::select('select FOUND_ROWS() total')[0]->total;

        return [
            'data' => $users,
            'total' => $total,
        ];
    }

    /**
     * @param array $attributes
     * @return User
     */
    public function create(array $attributes = []): ?User
    {
        return User::create($attributes);
    }

    /**
     * Subselect for last message between two users
     *
     * @return Expression
     */
    private function lastMessage(): Expression
    {
        return DB::raw('(
            SELECT
                messages.id
            FROM messages
            WHERE
                (author_id = user_friends.friend_id AND receiver_id = user_friends.user_id) OR
                (author_id = user_friends.user_id AND receiver_id = user_friends.friend_id)
            ORDER BY messages.id desc
            LIMIT 1
        )');
    }
}
