<?php

namespace App\Repositories;
use App\Country;

/**
 * Class CountryRepository
 * @package App\Repositories
 */
class CountryRepository
{
    /**
     * @param array $attributes
     * @return Country|null
     */
    public function create(array $attributes): ?Country
    {
        return Country::create($attributes);
    }
}