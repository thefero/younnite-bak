<?php

namespace App\Repositories;

use App\Message;
use App\User;
use Illuminate\Contracts\Pagination\Paginator;

/**
 * Class MessageRepository
 * @package App\Repositories
 */
class MessageRepository
{
    /**
     * Get a paginated list of messages sent from/to the logged in user
     *
     * If $partner is specified, the list is limited between the logged
     * user and the $partner
     *
     * $filters are used to add the request query params/filters to the
     * paginator links
     *
     * @param array $filters
     * @param User|null $partner
     * @return Paginator
     * @static
     */
    public static function myMessages(array $filters = [], ?User $partner = null): Paginator
    {
        $loggedUserIsAuthor = [ [ 'author_id', \Auth::user()->id ] ];
        $loggedUserIsReceiver = [ [ 'receiver_id', \Auth::user()->id ] ];

        $messages = Message::orderBy('messages.created_at', 'desc');

        if ($partner) {
            $loggedUserIsAuthor[] = [ 'receiver_id', $partner->id ];
            $loggedUserIsReceiver[] = [ 'author_id', $partner->id ];
        } else {
            $messages->with([ 'author', 'receiver' ]);
        }

        return $messages
            ->where($loggedUserIsAuthor)->orWhere($loggedUserIsReceiver)
            ->paginate(20)->appends($filters);
    }

    /**
     * Create a new Message model and store it in the database
     *
     * If no author_id is specified in the $attributes array,
     * it falls back the the logged user id
     *
     * @param array $attributes
     * @return Message
     * @static
     */
    public static function create(array  $attributes = []): ?Message
    {
        if (! isset($attributes['author_id']) || ! $attributes['author_id']) {
            $attributes['author_id'] = \Auth::user()->id;
        }

        return Message::create($attributes);
    }
}
