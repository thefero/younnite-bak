<?php

namespace App\Repositories;

use App\Company;
use Illuminate\Contracts\Pagination\Paginator;

/**
 * Class CompanyRepository
 * @package App\Repositories
 */
class CompanyRepository
{
    /**
     * @return Paginator
     */
    public static function all(): Paginator
    {
        return Company::with([
            'creator.avatars',
            'creator.covers',
            'city.country',
            'avatars',
            'covers',
            'activityDomains'
        ])->paginate(10);
    }

    /**
     * @param array $attributes
     * @return Company|null
     */
    public function create(array $attributes): ?Company
    {
        return Company::create($attributes);
    }
}