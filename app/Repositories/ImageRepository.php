<?php

namespace App\Repositories;

use App\Image;

/**
 * Class ImageRepository
 * @package App\Repositories
 */
class ImageRepository
{
    /**
     * Create a new Image model and store it in the database
     *
     * @param array $attributes
     * @return Image
     * @static
     */
    public static function create(array  $attributes = []): ?Image
    {
        return Image::create($attributes);
    }
}