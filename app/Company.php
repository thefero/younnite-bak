<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
* Class Company
 * @package App
*/
class Company extends Model
{
    /** @var array $attributes */
    protected $attributes = [
        'description' => ''
    ];

    /**
     * Get the user that created the company
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * @return BelongsToMany
     */
    public function avatars(): BelongsToMany
    {
        return $this->belongsToMany(
            Image::class,
            'company_avatars',
            'company_id',
            'image_id'
        )
            ->withTimestamps()
            ->using(CompanyAvatar::class);
    }

    /**
     * @return BelongsToMany
     */
    public function covers(): BelongsToMany
    {
        return $this->belongsToMany(
            Image::class,
            'company_covers',
            'company_id',
            'image_id'
        )
            ->withTimestamps()
            ->using(CompanyCover::class);
    }

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(
            City::class,
            'city_id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function activityDomains(): BelongsToMany
    {
        return $this->belongsToMany(
            ActivityDomain::class,
            'company_activity_domains',
            'company_id',
            'activity_domain_id'
        )
            ->withTimestamps()
            ->using(CompanyActivityDomain::class);
    }
}
