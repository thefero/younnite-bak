<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class City
 * @package App
 */
class City extends Model
{
    protected $fillable = [ 'name' ];

    /**
     * @return BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return HasMany
     */
    public function highSchools(): HasMany
    {
        return $this->hasMany(HighSchool::class);
    }

    /**
     * @return HasMany
     */
    public function colleges(): HasMany
    {
        return $this->hasMany(College::class);
    }
}
