<?php

namespace App\Services\Util;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait QueryParamsTransformer
 * @package App\Decorators
 */
class QueryParamsTransformer
{
    /** @var string */
    private const FILTER_PREFIX = 'where_';

    /** @var string Limit key name */
    private const LIMIT         = 'limit';

    /** @var string order_by key name */
    private const ORDER_BY      = 'order_by';

    /** @var string order_dir key name */
    private const ORDER_DIR     = 'order_dir';

    /** @var string filters key name */
    private const FILTERS       = 'filters';

    /**
     * @param array $params
     * @return array
     */
    public static function normalizeRequest(array $params): array
    {
        foreach ($params as $key => &$param) {
            if (is_array($param)) {
                $param = static::normalizeRequest($param);
                continue;
            }

            $param = trim($param);

            if ($key === 'last' || $key === 'first') {
                $params[self::LIMIT]     = $param;
                $params[self::ORDER_BY]  = Model::CREATED_AT;

                $params[self::ORDER_DIR] = $key === 'last' ? 'desc' : 'asc';

                unset($params[$key]);
            } else if (strpos($key, self::FILTER_PREFIX) === 0) {
                if (! isset($params[self::FILTERS])) {
                    $params[self::FILTERS] = [];
                }

                $params[self::FILTERS][substr($key, strlen(self::FILTER_PREFIX))] = $param;
                unset($params[$key]);
            } else if ($key == 'include') {
                $param = explode('|', $param);
            } else if (strpos($param, '|')) {
                $param = explode('|', $param);
            }
        }

        return $params;
    }
}
