<?php

namespace App\Services;

use Image;
use Illuminate\Support\Facades\Storage;

/**
 * Class ProjectImage
 * @package App\Services
 */
class ProjectImage
{
    /** @var ImageService $imageService */
    private $imageService;

    /**
     * ProjectImage constructor.
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @param string $path
     * @return \Generator
     */
    public function resizeBanner(string $path)
    {
        Storage::makeDirectory('public/project/banners');

        $coverDir = 'app/public/project/banners/';
        $coverName = $coverDir . uniqid('', true);

        $image = Image::make($path);

        $sizes = [
            'xl' => [ 1110, 500 ],
            'l'  => [ 690, 311 ],
            'm'  => [ 429, 193 ],
            's'  => [ 266, 120 ],
        ];

        foreach ($sizes as $type => $variant) {
            $image
                ->resize($variant[0], null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->crop($image->width(), $variant[1], 0, floor(($image->height() - $variant[1]) / 2))
                ->save(storage_path($coverName . '_' . $type . '.jpg'));

            yield $this->imageService->create($image);
        }
    }
}
