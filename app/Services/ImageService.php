<?php

namespace App\Services;

use Intervention\Image\Image;

/**
 * Class ImageService
 * @package App\Services
 */
class ImageService
{

    /**
     * @param $path
     * @return string
     */
    private static function stripStoragePath($path): string
    {
        if (! strpos($path, storage_path('app/public'))) {
            return substr($path, strlen(storage_path('app/public')));
        }

        return $path;
    }

    /**
     * @param Image $image
     * @return \App\Image
     */
    public function create(Image $image): \App\Image
    {
        return \App\Image::create([
            'name'      => substr($image->basename, 0, strrpos($image->basename, '.')),
            'path'      => $image->basePath(),
            'url'       => url('storage' . static::stripStoragePath($image->basePath())),
            'width'     => $image->getWidth(),
            'height'    => $image->getHeight(),
            'extension' => substr($image->basename, strrpos($image->basename, '.') + 1),
            'size'      => $image->filesize(),
        ]);
    }
}