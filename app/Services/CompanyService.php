<?php

namespace App\Services;

use App\Company;
use App\Image;
use App\Repositories\CompanyRepository;

/**
 * Class CompanyService
 * @package App\Services
 */
class CompanyService
{
    /** @var CompanyRepository */
    private $companyRepository;

    /** @var CompanyImage $companyImage */
    private $companyImage;

    /**
     * CompanyService constructor.
     *
     * @param CompanyImage $companyImage
     * @param CompanyRepository $companyRepository
     */
    public function __construct(CompanyImage $companyImage, CompanyRepository $companyRepository)
    {
        $this->companyImage = $companyImage;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param array $attributes
     * @return Company|null
     */
    public function create(array $attributes): ?Company
    {
        /** @var Company $company */
        $company = $this->companyRepository->create([
            'creator_id'    => $attributes['creator_id'] ?? null,
            'name'          => $attributes['name'] ?? '',
            'description'   => $attributes['description'] ?? '',
            'city_id'       => $attributes['city_id'] ?? null,
        ]);

        if (! $company) {
            return null;
        }

        $avatars = [];
        $covers = [];

        if (! isset($attributes['avatar']) || ! $attributes['avatar']) {
            /** @var Image $avatar */
            foreach ($this->companyImage->generateDefaultAvatar($attributes) as $avatar) {
                $avatars[] = $avatar->id;
            }

            if (count($avatars)) {
                $company->avatars()->sync($avatars);
            }
        }

        if (isset($attributes['cover']) && $attributes['cover']) {
            /** @var Image $avatar */
            foreach ($this->companyImage->resizeCover($attributes) as $cover) {
                $covers[] = $cover->id;
            }

            if (count($covers)) {
                $company->covers()->sync($covers);
            }
        }

        return $company;
    }
}
