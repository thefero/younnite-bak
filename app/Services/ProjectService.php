<?php

namespace App\Services;


use App\Image;
use App\Project;
use App\Repositories\ProjectRepository;

/**
 * Class ProjectService
 * @package App\Services
 */
class ProjectService
{
    /**
     * @var ProjectRepository $projectRepository
     */
    private $projectRepository;

    /** @var ProjectImage $projectImage */
    private $projectImage;

    /**
     * ProjectService constructor.
     * @param ProjectImage $projectImage
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectImage $projectImage, ProjectRepository $projectRepository)
    {
        $this->projectImage = $projectImage;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param array $attributes
     * @return Project|null
     */
    public function create(array $attributes): ?Project
    {
        /** @var Project $project */
        $project = $this->projectRepository->create([
            'user_id'      => $attributes['user_id'] ?? '',
            'title'        => $attributes['title'] ?? '',
            'subtitle'     => $attributes['subtitle'] ?? '',
            'description'  => $attributes['description'] ?? '',
            'start_date'   => $attributes['start_date'] ?? '',
            'start_time'   => $attributes['start_time'] ?? null,
            'end_date'     => $attributes['end_date'] ?? '',
            'end_time'     => $attributes['end_time'] ?? null,
        ]);

        if (! $project) {
            return null;
        }

        $banners = [];

        if (! empty($attributes['banner'])) {
            /** @var Image $avatar */
            foreach ($this->projectImage->resizeBanner($attributes['banner']) as $cover) {
                $banners[] = $cover->id;
            }

            if (count($banners)) {
                $project->banners()->sync($banners);
            }
        }

        return $project;
    }
}