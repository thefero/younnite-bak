<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Image;
use Intervention\Image\AbstractFont;

/**
 * Class UserImage
 * @package App\Services
 */
class UserImage
{
    private const PROFILE_IMAGE_COLORS = [
        '#007bff' => '#ffffff',
        '#6c757d' => '#ffffff',
        '#28a745' => '#ffffff',
        '#dc3545' => '#ffffff',
        '#ffc107' => '#343a40',
        '#17a2b8' => '#ffffff',
        '#f8f9fa' => '#343a40',
        '#343a40' => '#ffffff',
        '#ffffff' => '#343a40',
    ];

    /** @var ImageService $imageService */
    private $imageService;

    /**
     * UserImage constructor.
     *
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @param array $attributes
     * @return \Generator
     */
    public function generateDefaultAvatar(array $attributes): \Generator
    {
        $backgroundColor = array_keys(static::PROFILE_IMAGE_COLORS)
            [ rand(0, count(static::PROFILE_IMAGE_COLORS) - 1) ];

        $textColor = static::PROFILE_IMAGE_COLORS[$backgroundColor];

        Storage::makeDirectory('public/user/avatar');
        $avatarName = 'app/public/user/avatar/' . uniqid('', true);

        $variants = array_reverse(array_sort([ 128, 64, 32 ]));

        /** @var Image|\Intervention\Image\Image $image */
        $image = Image::canvas(300, 300, $backgroundColor)
            ->text(
                strtoupper($attributes['first_name'][0]) . strtoupper($attributes['last_name'][0]),
                150,
                150,
                function (AbstractFont $font) use ($textColor) {
                    $font->file(base_path() . '/resources/fonts/Roboto-Regular.ttf');
                    $font->size(150);
                    $font->color($textColor);
                    $font->align('center');
                    $font->valign('middle');
                }
            )
            ->save(storage_path($avatarName . '_300.png'));
        yield $this->imageService->create($image);

        /** @var int $variant */
        foreach ($variants as $variant) {
            $image->resize($variant, $variant)->save(
                storage_path("{$avatarName}_{$variant}." .
                    substr($image->basename, strrpos($image->basename, '.') + 1))
            );

            yield $this->imageService->create($image);
        }
    }

    /**
     * @param array $attributes
     * @return \Generator
     */
    public function resizeCover(array $attributes): \Generator
    {
        Storage::makeDirectory('public/user/cover');
        $coverDir = 'app/public/user/cover/';

        $coverName = $coverDir . uniqid('', true);

        $image = Image::make($attributes['cover']);

        $sizes = [
            'xl' => [ 1110, 500 ],
            'l'  => [ 690, 311 ],
            'm'  => [ 429, 193 ],
            's'  => [ 266, 120 ],
        ];

        foreach ($sizes as $type => $variant) {
            $image
                ->resize($variant[0], null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->crop($image->width(), $variant[1], 0, floor(($image->height() - $variant[1]) / 2))
                ->save(storage_path($coverName . '_' . $type . '.jpg'));

            yield $this->imageService->create($image);
        }
    }
}
