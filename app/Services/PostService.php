<?php

namespace App\Services;

use App\Repositories\PostRepository;
use App\Services\Util\QueryParamsTransformer;

/**
 * Class PostService
 * @package App\Services
 */
class PostService
{
    /** @var PostRepository $postRepository */
    private $postRepository;

    /**
     * PostService constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param string $url
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public function all(string $url, array $params)
    {
        $params = QueryParamsTransformer::normalizeRequest($params);

        return $this->postRepository->all();
    }
}