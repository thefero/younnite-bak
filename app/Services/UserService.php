<?php

namespace App\Services;

use App\Image;
use App\Message;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /**
     * @var UserImage $userImage
     */
    private $userImage;

    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserImage $userImage
     * @param UserRepository $userRepository
     */
    public function __construct(UserImage $userImage, UserRepository $userRepository)
    {
        $this->userImage = $userImage;
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $currentUrl
     * @param int $perPage
     * @param array $query
     *
     * @return Paginator
     */
    public function getFriendsWithLastMessage(string $currentUrl = '', int $perPage = 10, array $query = []): Paginator
    {
        $page = isset($query['page']) && $query['page'] ? $query['page'] : 1;
        extract($this->userRepository->getFriendsWithLastMessage($currentUrl, $perPage, $query));

        /** @var Collection $data */
        foreach ($data as $key => $user) {
            $data[$key] = User::make((array) $user)->load('avatars');

            if ($user->message_id) {
                $data[$key]->setRelation('lastMessage', Message::make([
                    'id' => $user->message_id,
                    'author_id' => $user->message_author_id,
                    'receiver_id' => $user->message_receiver_id,
                    'body' => $user->message_body,
                    'created_at' => $user->message_created_at,
                ]));
            }
        };

        /** @var int $total */
        return new LengthAwarePaginator($data, $total, $perPage, $page, [
            'path'  => $currentUrl,
            'query' => $query,
        ]);
    }

    /**
     * @param array $attributes
     * @return User|null
     */
    public function create(array $attributes): ?User
    {
        /** @var User $user */
        $user = $this->userRepository->create([
            'first_name'    => $attributes['first_name'] ?? '',
            'last_name'     => $attributes['last_name'] ?? '',
            'middle_name'   => $attributes['middle_name'] ?? '',
            'title'         => $attributes['title'] ?? '',
            'email'         => $attributes['email'] ?? '',
            'password'      => $attributes['password'] ?? '',
        ]);

        if (! $user) {
            return null;
        }

        $avatars = [];
        $covers = [];

        if (! isset($attributes['avatar']) || ! $attributes['avatar']) {
            /** @var Image $avatar */
            foreach ($this->userImage->generateDefaultAvatar($attributes) as $avatar) {
                $avatars[] = $avatar->id;
            }

            if (count($avatars)) {
                $user->avatars()->sync($avatars);
            }
        }

        if (isset($attributes['cover']) && $attributes['cover']) {
            /** @var Image $avatar */
            foreach ($this->userImage->resizeCover($attributes) as $cover) {
                $covers[] = $cover->id;
            }

            if (count($covers)) {
                $user->covers()->sync($covers);
            }
        }

        return $user;
    }
}
