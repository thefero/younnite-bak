<?php

namespace App;

use App\Events\MessageCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Message
 * @package App
 */
class Message extends Model
{
    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function receiver(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    protected static function boot(): void
    {
        parent::boot();
//        self::created(function($model) {
//            MessageCreated::dispatch($model);
//        });
    }
}
