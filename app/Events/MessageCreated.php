<?php

namespace App\Events;

use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Message;
use App\User;

/**
 * Class MessageCreated
 * @package App\Events
 */
class MessageCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var Message $message */
    public $message;

    /** @var UserResource $author */
    public $author;

    /**
     * Create a new event instance.
     *
     * @param Message $message
     * @param User $author
     */
    public function __construct(Message $message, User $author)
    {
        $this->message = $message;
        $this->author = UserResource::make($author->load('avatars'));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('message-' . $this->message->receiver_id);
    }
}
