<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'first_name', 'last_name', 'middle_name', 'title', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return string
     */
    public function getNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Retrieve the users that have the current user as a friend
     *
     * @return BelongsToMany
     */
    public function friendOf(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'user_friends',
            'friend_id',
            'user_id'
        )
            ->withTimestamps()
            ->using(Friend::class);
    }

    /**
     * Retrieve the user's friends
     *
     * @return BelongsToMany
     */
    public function friends(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'user_friends',
            'user_id',
            'friend_id'
        )
            ->withTimestamps()
            ->using(Friend::class);
    }

    /**
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(
            Post::class,
            'user_id',
            'id'
        );
    }

    /**
     * @return HasMany
     */
    public function authoredMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'author_id');
    }

    /**
     * @return HasMany
     */
    public function receivedMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }

    /**
     * @return BelongsToMany
     */
    public function avatars(): BelongsToMany
    {
        return $this->belongsToMany(
            Image::class,
            'user_avatars',
            'user_id',
            'image_id'
        )
            ->withTimestamps()
            ->using(UserAvatar::class);
    }

    /**
     * @return BelongsToMany
     */
    public function covers(): BelongsToMany
    {
        return $this->belongsToMany(
            Image::class,
            'user_covers',
            'user_id',
            'image_id'
        )
            ->withTimestamps()
            ->using(UserCover::class);
    }

    /**
     * @return BelongsToMany
     */
    public function highSchools(): BelongsToMany
    {
        return $this->belongsToMany(
            HighSchool::class,
            'user_high_schools',
            'user_id',
            'high_school_id'
        )
            ->withTimestamps()
            ->using(UserHighSchool::class);
    }

    /**
     * @return BelongsToMany
     */
    public function colleges(): BelongsToMany
    {
        return $this->belongsToMany(
            College::class,
            'user_colleges',
            'user_id',
            'college_id'
        )
            ->withTimestamps()
            ->using(UserCollege::class);
    }
}
