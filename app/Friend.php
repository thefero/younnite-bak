<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class Friend
 * @package App
 */
class Friend extends Pivot
{
    protected $table = 'user_friends';

    /**
     * @return HasMany
     */
    public function authoredMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'id', 'author_id');
    }

    /**
     * @return HasMany
     */
    public function receivedMessages(): HasMany
    {
        return $this->hasMany(Message::class,  'id', 'receiver_id');
    }

    public function getMessagesAttribute()
    {
        return $this->authoredMessages->merge($this->receivedMessages);
    }
}
