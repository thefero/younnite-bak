import Vue from "vue";

Vue.mixin({
    methods: {
        fromNowDate(date) {
            return moment(date).fromNow();
        }
        , calendarDate(date) {
            return moment(date).format('MMM Do');
        }
        , fullDate(date) {
            let hour = moment(date).format('HH:mm:ss')
                , format = 'ddd, MMM Do, YYYY';

            if (hour !== '00:00:00') {
                format += ' HH:mm';
            }

            return moment(date).format(format);
        }
    }
});