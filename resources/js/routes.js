import VueRouter from "vue-router";

import Settings from './views/Settings';
import Profile from './views/Profile';
import Browse from './views/Browse';
import Home from './views/Home';
import Add from './views/project/Add';

export default new VueRouter({
    mode: 'history'
    , routes: [
        {
            path: '/'
            , name: 'home'
            , component: Home
        }, {
            path: '/add'
            , name: 'project.add'
            , component: Add
        }, {
            path: '/browse'
            , name: 'browse'
            , component: Browse
        }, {
            path: '/profile'
            , name: 'profile'
            , component: Profile
        }, {
            path: '/settings'
            , name: 'settings'
            , component: Settings
        }
    ]
})