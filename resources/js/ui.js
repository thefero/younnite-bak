require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import './helpers';
import router from './routes';
import InfiniteLoading from 'vue-infinite-loading/src/components/InfiniteLoading';
import BootstrapVue from 'bootstrap-vue';

import Navbar from './components/layout/Navbar';
import Editor from './components/form/Editor';
import Upload from './components/form/Upload';
import resize from 'vue-resize-directive';

Vue.use(VueRouter);
Vue.use(BootstrapVue);

Vue.component('infinite-loading', InfiniteLoading);

Vue.directive('resize', resize);

new Vue({
    el: '#app'
    , data: {
        files: {}
    }
    , components: {
        Navbar
        , InfiniteLoading
        , Editor
        , Upload
    }
    , router
});
