import {
    ADD_MESSAGES_TO_CONVERSATION,
    ADD_MESSAGE_TO_CONVERSATION,
    SET_CONVERSATION_VISIBLE,
    SET_HIDE_BEFORE_ADDING,
    REMOVE_CONVERSATION,
    HIDE_CONVERSATIONS,
    OPEN_CONVERSATION,
    TOGGLE_MINIMIZE,
} from "../mutaion-types";

import Vue from "vue";

const initConversation = conversation => {
    const fields = {
        id: 0
        , participant: {}
        , hidden: false
        , minimized: false
        , newMessages: []
    };

    for (let field in fields) {
        if (fields.hasOwnProperty(field)) {
            Vue.set(
                conversation
                , field
                , typeof conversation[field] !== 'undefined' ? conversation[field] : fields[field]
            );
        }
    }
};

const hideConversations = (conversations, count) => {
    for (let i = 0, conversationsLength = conversations.length, j = 0; i < conversationsLength; i++) {
        if (! conversations[i].hidden) {
            if (j < count) {
                conversations[i].hidden = !! ++j;
            } else {
                break;
            }
        }
    }
};

export default {
    state: {
        conversations: []
        , hideBeforeAdding: 0
    }
    , mutations: {
        [ OPEN_CONVERSATION ] (state, participant) {
            if (state.conversations.findIndex(o => o.id === participant.id) !== -1) return;

            let conversation = {
                participant
                , id: participant.id
            };
            initConversation(conversation);
            if (state.hideBeforeAdding) {
                this.commit(HIDE_CONVERSATIONS, state.hideBeforeAdding);
            }

            state.conversations.push(conversation);
        }
        , [ REMOVE_CONVERSATION ] (state, conversation) {
            state.conversations.splice(state.conversations.findIndex(o => o.id === conversation.participant.id), 1);
        }
        , [ SET_HIDE_BEFORE_ADDING ] (state, count) {
            state.hideBeforeAdding = count;
        }
        , [ HIDE_CONVERSATIONS ] (state, count) {
            hideConversations(state.conversations, count);
        }
        , [ TOGGLE_MINIMIZE ] (state, data) {
            const conversation = state.conversations.find(c => c.id === data.conversation.id);

            conversation.minimized = typeof data.minimize !== 'undefined' ?
                !! data.minimize :
                ! conversation.minimized;
        }
        , [ SET_CONVERSATION_VISIBLE ] (state, witch) {
            const hiddenConversations = state.conversations.filter(item => item.hidden).reverse();

            if (hiddenConversations.length) {
                if (witch === 'small' && hiddenConversations[0].minimized) {
                    hiddenConversations[0].hidden = false;
                } else if (witch === 'large') {
                    hiddenConversations[0].hidden = false;
                }
            }
        }
    }
    , getters: {
        activeConversations: state => state.conversations
    }
    , actions: {
        addMessageToConversation(store, conversation) {
            const index = store.state.conversations.findIndex(o => o.id === conversation.participant.id);

            /** Conversation does not exist */
            if (index === -1) {
                store.commit(OPEN_CONVERSATION, conversation.participant);
            } else { /** Conversation exists */
                store.commit(ADD_MESSAGE_TO_CONVERSATION, {
                    message: conversation.message
                    , participant: conversation.participant
                });
            }
        }
        , sendMessage(store, data) {
            window.axios.post(`/api/v1/messages`, {
                message: data.message.body
                , to: data.participant.id
            }).then(response => {
                console.log(response)
            });

            store.commit(ADD_MESSAGE_TO_CONVERSATION, {
                message: data.message
                , participant: data.participant
            });
        }
    }
}