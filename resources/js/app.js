require('./bootstrap');

import Vue from 'vue';
import store from './store';
import VueRouter from 'vue-router';
import './helpers';
import router from './routes';
import InfiniteLoading from 'vue-infinite-loading/src/components/InfiniteLoading';
import BootstrapVue from 'bootstrap-vue';

import Conversations from './components/chat/Conversations';
import Friends from './components/chat/Friends';
import Navbar from './components/layout/Navbar';
import resize from 'vue-resize-directive';

Vue.use(VueRouter);
Vue.use(BootstrapVue);

Vue.component('infinite-loading', InfiniteLoading);

Vue.directive('resize', resize);

Vue.prototype.eventHub = new Vue();

new Vue({
    el: '#app'
    , components: {
        Conversations
        , Friends
        , Navbar
    }
    , router
    , store
});
