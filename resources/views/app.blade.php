<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Younnite') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>
        window.App = {!! json_encode([ 'user' => $user ?? null ]); !!};
    </script>
</head>
<body>
    <div id="app">
        <navbar></navbar>
        <main class="container">
            <router-view></router-view>
        </main>

        <conversations></conversations>
        <friends url="/api/v1/friends" class="fixed-sidebar fixed-sidebar-right"></friends>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
