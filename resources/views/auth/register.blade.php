@extends('layouts.static')

@section('content')
<div class="container">

    <div class="d-flex mx-auto" style="width: 100%; margin-top: 2rem;">
        <div class="side-panels"></div>
        <div style="width: 550px;">
            <panel header="Register" :actions="false">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="first_name" class="col-form-label">First name</label>
                                <input type="text" id="first_name" name="first_name" class="form-control" placeholder="John">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="last_name" class="col-form-label">Last name</label>
                                <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Doe">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="john.doe@email.com">
                        </div>

                        <div class="form-group">
                            <label for="school">Educational institution</label>
                            <input id="school" name="school" class="form-control" placeholder="Select one">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="birth_day">Day</label>
                                <select id="birth_day" name="birth_day" class="form-control">
                                    <option value="1">1</option>
                                    <option value="3">2</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="birth_month">Month</label>
                                <select id="birth_month" name="birth_month" class="form-control">
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="birth_year">Year</label>
                                <input type="text" id="birth_year" name="birth_year" class="form-control" placeholder="Birth year">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control" name="password" required>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                                Register
                            </button>
                        </div>
                    </form>
                </div>
            </panel>
        </div>
        <div class="side-panels"></div>
    </div>
</div>
@endsection
