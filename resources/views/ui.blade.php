<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Younnite') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>
        window.App = {!! json_encode([ 'user' => $user ?? null ]); !!};
    </script>
</head>
<body>
    <div id="app">
        <navbar></navbar>
        <main class="container">
            <div class="form-group">
                <editor></editor>
            </div>
            <div class="form-group">
                <upload name="banner" v-model="files"></upload>
            </div>
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/ui.js') }}"></script>
</body>
</html>
