<?php

namespace Tests;

use App\User;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestResponse;
use Laravel\Passport\ClientRepository;

/**
 * Class AuthenticatedTestCase
 * @package Tests
 */
class AuthenticatedTestCase extends TestCase
{
//    use DatabaseMigrations;

    /** @var User $user */
    protected $user;

    protected $scopes = [];

    protected $headers = [];

    public function setUp()
    {
        parent::setUp();

        $clientRepository = new ClientRepository();
        $client = $clientRepository->createPersonalAccessClient(
            null,
            'Test personal access client',
            config('app.url')
        );

        $currentTime = new \DateTime();

        \DB::table('oauth_personal_access_clients')
            ->insert([
                'client_id' => $client->id,
                'created_at' => $currentTime,
                'updated_at' => $currentTime,
            ]);

        $this->user = factory(User::class)->create();

        $token = $this->user->createToken('TestToken', $this->scopes)->accessToken;

        $this->headers['Accept'] = 'application/json';
        $this->headers['Authorization'] = 'Bearer ' . $token;
    }

    /**
     * @param string $uri
     * @param array $headers
     * @return TestResponse
     */
    public function get($uri, array $headers = []): TestResponse
    {
        return parent::get($uri, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $headers
     * @return TestResponse
     */
    public function getJson($uri, array $headers = []): TestResponse
    {
        return parent::getJson($uri, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function post($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::post($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function postJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::postJson($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function put($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::put($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function putJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::putJson($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function patch($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::patch($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function patchJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::patchJson($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function delete($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::delete($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return TestResponse
     */
    public function deleteJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::deleteJson($uri, $data, array_merge($this->headers, $headers));
    }
}