<?php

namespace Tests\Feature;

use App\Message;
use App\Post;
use App\Project;
use App\User;
use Illuminate\Http\Response;
use Tests\AuthenticatedTestCase;

/**
 * Class UsersAPITest
 * @package Tests\Feature
 */
class UsersAPITest extends AuthenticatedTestCase
{
    protected $scopes = [];

    public function testSelf()
    {
        $this->get('/api/v1/self')
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['data' => [
                'first_name'   => $this->user->first_name,
                'last_name'    => $this->user->last_name,
                'display_name' => $this->user->first_name . ' ' . $this->user->last_name,
                'title'        => $this->user->title,
                'avatar'       => $this->user->avatar,
                'email'        => $this->user->email,
            ]]);
    }

    public function testFriends()
    {
        $friend1 = factory(User::class)->create();
        $friend2 = factory(User::class)->create();

        $this->user->friends()->attach($friend1);
        $this->user->friends()->attach($friend2);

        $this->get('/api/v1/friends')
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [],
                'links' => [],
                'meta' => [],
            ])
            ->assertJsonMissing([
                'data' => [
                    ['last_message' => []],
                    ['last_message' => []],
                ],
            ]);

        $createdAt = $updatedAt = microtime(true);

        Message::create([
            'author_id' => $friend1->id,
            'receiver_id' => $this->user->id,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
            'body' => 'Some content here',
        ]);

        $createdAt = $updatedAt = microtime(true);

        Message::create([
            'author_id' => $this->user->id,
            'receiver_id' => $friend2->id,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
            'body' => 'Some content here',
        ]);

        $this->get('/api/v1/friends')
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [
                    ['last_message' => []],
                    ['last_message' => []],
                ],
            ]);
    }

    public function testShow()
    {
        $randomUser = factory(User::class)->create();

        $this->get('/api/v1/user/' . $randomUser->id)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['data' => [
                'first_name'   => $randomUser->first_name,
                'last_name'    => $randomUser->last_name,
                'display_name' => $randomUser->first_name . ' ' . $randomUser->last_name,
                'title'        => $randomUser->title,
                'avatar'       => $randomUser->avatar,
                'email'        => $randomUser->email,
            ]]);
    }

    public function testUserWithPosts()
    {
        $this->get('/api/v1/user/' . $this->user->id . '?include=posts')
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['data' => [
                'display_name' => $this->user->first_name . ' ' . $this->user->last_name,
                'avatar' => $this->user->avatar,
                'posts' => [
                    'meta' => [
                        'total' => 0
                    ],
                ],
            ]]);

        /** @var Project $project */
        $project = Project::create([
            'user_id' => $this->user->id,
            'title' => 'My proj 1',
            'subtitle' => 'Subtitle',
            'description' => 'Some description',
            'start_date' => new \DateTime(),
            'end_date' => new \DateTime(),
        ]);

        Post::create([
            'user_id' => $this->user->id,
            'what' => 'project',
            'post_id' => $project->id,
        ]);

        $this->get('/api/v1/user/' . $this->user->id . '?include=posts')
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['data' => [
                'display_name' => $this->user->first_name . ' ' . $this->user->last_name,
                'title' => $this->user->title,
                'posts' => [
                    'meta' => [
                        'total' => 1
                    ],
                ],
            ]]);
    }
}