<?php

Auth::routes();

Route::prefix('/auth')->group(function() {
    Route::get('facebook/login', 'Auth\LoginController@facebook')->name('facebook.login');
    Route::get('facebook/callback', 'Auth\LoginController@handleFacebookCallback')->name('facebook.login.callback');

    Route::get('github/login', 'Auth\LoginController@github')->name('github.login');
    Route::get('github/callback', 'Auth\LoginController@handleGithubCallback')->name('github.login.callback');
});

Route::get('/ui', 'UIController');
Route::post('/temp/upload', 'UploadController');

Route::get('/{any}', 'AppController@index')
    ->middleware([ 'auth' ])
    ->where('any', '.*');
