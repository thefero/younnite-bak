<?php

use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('chat', function ($user) {
    return \App\Http\Resources\UserResource::make($user);
});

Broadcast::channel('message-{id}', function ($user, $id) {
    if ((int) $user->id === (int) $id) {
        return \App\Http\Resources\UserResource::make($user);
    }
});
