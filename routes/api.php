<?php

Route::get('self', 'UserController@self')->name('api.self');

Route::get('user/{id}', 'UserController@show')->name('api.user');
Route::get('messages', 'MessageController@list')->name('api.messages');
Route::post('messages', 'MessageController@create')->name('api.create_messages');
Route::get('friends', 'UserController@friends')->name('api.friends');
Route::get('projects', 'UserController@friends')->name('api.friends');

Route::get('posts', 'PostController@list')->name('api.posts.list');
Route::post('post', 'PostController@create')->name('api.posts.create');
Route::get('companies', 'CompanyController@list')->name('api.companies');
