const mix = require('laravel-mix');

const PUBLIC_PATH = 'public';

mix.js('resources/js/app.js', `${PUBLIC_PATH}/js`)
    .js('resources/js/ui.js', `${PUBLIC_PATH}/js`)
    .sass('resources/sass/app.scss', `${PUBLIC_PATH}/css`)
    .sass('resources/sass/editor-content.scss', `${PUBLIC_PATH}/css`)
    .copyDirectory(path.resolve(__dirname, 'node_modules/tinymce/skins'), `${PUBLIC_PATH}/tinymce/skins`)
    .copyDirectory(path.resolve(__dirname, 'resources/tinymce/skins'), `${PUBLIC_PATH}/tinymce/skins`)
    .webpackConfig({
        module: {
            rules: [
                {
                    test: require.resolve('tinymce/tinymce'),
                    loaders: [
                        'imports-loader?this=>window',
                        'exports-loader?window.tinymce'
                    ]
                },
                {
                    test: /tinymce\/(themes|plugins)\//,
                    loaders: [
                        'imports-loader?this=>window'
                    ]
                }
            ]
        }
    });
